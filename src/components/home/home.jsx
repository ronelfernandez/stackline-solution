import React from 'react';
import Product from '../product/product';
import Header from '../header/header';
import Tags from '../tags/tags';
import Navigation from '../navigation/navigation';
import Chart from '../chart/chart';
import Table from '../table/table';
import Overview from '../overview/overview';
import './home.css';

export default ({
  product: { title, subtitle, brand, retailer, image, tags, sales, details, reviews },
  ui: { activeView }
}) => {
  React.useEffect(() => {
    document.title = title
  }, [title]);
  return (
    <div className="home">
      <Header className="header" />
      <div className="left-nav">
        <Product
          title={title}
          subtitle={subtitle}
          image={image}
          className="product"
        />
        <Tags className="tags" tags={tags} />
        <Navigation className="navigation" />
      </div>
      <div className="chart-table">
        {activeView === 'sales' ? (
          <>
            <Chart className="chart" sales={sales} />
            <Table sales={sales} />
          </>
        ) : (
          <Overview className="overview" title={title} details={details} brand={brand} retailer={retailer} reviews={reviews} />
        )}
      </div>
    </div>
  );
};
