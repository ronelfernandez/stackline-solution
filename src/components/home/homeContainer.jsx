import React from 'react';
import getProductAction from '../../actions/getProductAction';

import { connect } from 'react-redux';
import Home from './home';

export const HomeContainer = ({ product, getProductAction, ui }) => {
  React.useEffect(() => {
    getProductAction('B007TIE0GQ');
  }, [getProductAction]);

  return <Home product={product} ui={ui} />;
};

const mapStateToProps = state => {
  return {
    product: state.product,
    ui: state.ui
  };
};

const mapDispatchToProps = {
  getProductAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);
