import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

import moment from 'moment';

const options = {
  title: {
    text: 'Retail Sales',
    align: 'left',
  },
  yAxis: {
    visible: false,
    min: 500000,
  },
  xAxis: {
    type: 'category',
    categories: [
      ...Array(12)
        .fill(0)
        .map((_, index) =>
          moment(new Date(2020, index, 1))
            .format('MMM')
            .toUpperCase()
        ),
    ],
    labels: {
      style: {
        color: 'var(--secondary-color)',
        fontSize: '16px',
        padding: '10px',
      },
    },
  },
  plotOptions: {
    series: {
      marker: {
        enabled: false,
      },
    },
  },
  legend: {
    enabled: false,
  },
};

export default ({ sales, ...rest }) => {
  const series = sales.reduce(
    (acc, { weekEnding, retailSales, wholesaleSales }) => {
      const month = moment(weekEnding).month();
      acc[0].data[month] = acc[0].data[month] + retailSales;
      acc[1].data[month] = acc[1].data[month] + wholesaleSales;
      return acc;
    },
    [
      { name: 'Retail Sales', data: Array(12).fill(0), type: 'spline' },
      { name: 'Wholesale Sales', data: Array(12).fill(0), type: 'spline' },
    ]
  );
  return (
    <div {...rest}>
      <HighchartsReact
        highcharts={Highcharts}
        containerProps={{ className: 'chartContainer' }}
        options={{
          ...options,
          series,
        }}
      />
    </div>
  );
};
