import React from "react";
import stackline from '../../images/stackline.png'
export default (props) => {
  return <div {...props}><img alt="stackline" src={stackline}></img>Stackline</div>;
};
