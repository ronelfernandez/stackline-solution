import React from 'react';

export default ({ title, subtitle, image, ...rest }) => {
  return (
    <div {...rest}>
      <img src={image} alt={title}/>
      <h1 className="title">{title}</h1>
      <span className="subtitle">{subtitle}</span>
    </div>
  );
};
