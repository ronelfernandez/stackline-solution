import React from 'react';

export default ({ title, brand, retailer, details, reviews, ...rest }) => {
  return (
    <div {...rest}>
      <h3>{title}</h3>
      <span>By <b>{brand}</b></span>
      <span>Sold by <b>{retailer}</b></span>
      <ul className="details">
        {details.map((detail, index) => (
          <li key={index}>{detail}</li>
        ))}
      </ul>
      <h3>Reviews</h3>
      <ul className="reviews">
        {reviews.map(({ customer, review, score }, index) => (
          <li key={index}>
            <span>
              {[...new Array(score)].map((_, index) => (
                <i className="material-icons" key={index}>
                  star
                </i>
              ))}
            </span>
            <span>{customer}</span>
            <span>{review}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};
