import React from "react";

export default ({tags, ...rest}) => {
    return (
        <ul {...rest}>
            {
                tags.map(tag => (
                    <li key={tag}>{tag}</li>
                ))
            }
        </ul>
    )
}