import React from 'react';
import numeral from 'numeral';
import moment from 'moment';
import { connect } from 'react-redux';
import classNames from 'classnames';

import setSortConfig, { ASCENDING } from '../../actions/setSortConfig';

export const Table = ({
  sales,
  rest,
  ui: { sortColumn, sortOrder },
  setSortConfig,
}) => {
  const data = sales.sort((a, b) => {
    if (sortColumn === 'weekEnding') {
      if (sortOrder === ASCENDING) {
        return new Date(a.weekEnding) - new Date(b.weekEnding);
      }
      return new Date(b.weekEnding) - new Date(a.weekEnding);
    }
    const sortValue = columnName => {
      if (sortOrder === ASCENDING) {
        return a[columnName] - b[columnName];
      }
      return b[columnName] - a[columnName];
    };
    return sortValue(sortColumn);
  });

  const handleSort = colName => () => {
    setSortConfig(colName);
  };

  const SortIcon = ({ columnName }) => {
    const icon = sortOrder === ASCENDING ? 'expand_more' : 'expand_less';
    return (
      <i
        className={classNames(
          'material-icons',
          columnName === sortColumn ? 'selected' : undefined
        )}
      >
        {icon}
      </i>
    );
  };

  return (
    <div {...rest}>
      <table>
        <thead>
          <tr>
            <th align="left" onClick={handleSort('weekEnding')}>
              <span>
                WEEK ENDING
                <SortIcon columnName="weekEnding" />
              </span>
            </th>
            <th align="right" onClick={handleSort('retailSales')}>
              <span>
                <SortIcon columnName="retailSales" />
                RETAIL SALES
              </span>
            </th>
            <th align="right" onClick={handleSort('wholesaleSales')}>
              <span>
                <SortIcon columnName="wholesaleSales" />
                WHOLESALE SALES
              </span>
            </th>
            <th align="right" onClick={handleSort('unitsSold')}>
              <span>
                <SortIcon columnName="unitsSold" />
                UNIT SOLD
              </span>
            </th>
            <th align="right" onClick={handleSort('retailerMargin')}>
              <span>
                <SortIcon columnName="retailerMargin" />
                RETAILER MARGIN
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          {data.map(
            (
              {
                weekEnding,
                retailSales,
                wholesaleSales,
                unitsSold,
                retailerMargin,
              },
              index
            ) => (
              <tr key={index}>
                <td>{moment(weekEnding).format('MM-DD-YY')}</td>
                <td align="right">{numeral(retailSales).format('$0,0')}</td>
                <td align="right">{numeral(wholesaleSales).format('$0,0')}</td>
                <td align="right">{numeral(unitsSold).format('0,0')}</td>
                <td align="right">{numeral(retailerMargin).format('$0,0')}</td>
              </tr>
            )
          )}
        </tbody>
      </table>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    ui: state.ui,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setSortConfig: sortColumn => {
      dispatch(setSortConfig(sortColumn));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Table);
