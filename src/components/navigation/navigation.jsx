import React from 'react';
import { connect } from 'react-redux';
import setActiveView from '../../actions/setActiveView';

const SALES = 'sales';
const OVERVIEW = 'overview';

export const Navigation = ({ ui: { activeView }, setActiveView, ...rest }) => {
  const setActiveViewGHandler = view => () => {
    setActiveView(view);
  };
  return (
    <ul {...rest}>
      <li
        onClick={setActiveViewGHandler(OVERVIEW)}
        className={activeView === OVERVIEW ? 'selected' : undefined}
      >
        <i className="material-icons">house</i>OVERVIEW
      </li>
      <li
        onClick={setActiveViewGHandler(SALES)}
        className={activeView === SALES ? 'selected' : undefined}
      >
        <i className="material-icons">bar_chart</i>SALES
      </li>
    </ul>
  );
};

const mapStateToProps = state => {
  return {
    ui: state.ui
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setActiveView: activeView => {
      dispatch(setActiveView(activeView));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigation);
