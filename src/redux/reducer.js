import { GET_PRODUCT } from '../actions/getProductAction';
import { SET_ACTIVE_VIEW } from '../actions/setActiveView';
import { SET_SORT_CONFIG, ASCENDING } from '../actions/setSortConfig';
import initialState from './initialState';

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT: {
      return { ...state, product: action.payload };
    }
    case SET_ACTIVE_VIEW: {
      return {
        ...state,
        ui: {
          ...state.ui,
          activeView: action.activeView,
        },
      };
    }
    case SET_SORT_CONFIG: {
      return {
        ...state,
        ui: {
          ...state.ui,
          sortOrder: action.sortColumn === state.ui.sortColumn ? (state.ui.sortOrder === ASCENDING ? 'desc' : ASCENDING) : ASCENDING,
          sortColumn: action.sortColumn,
        },
      };
    }
    default: {
      return state;
    }
  }
};
