export default {
  product: {
    sales: [],
    tags: [],
    details: [],
    reviews: [],
  },
  ui: {
    activeView: 'sales',
    sortColumn: 'weekEnding',
    sortOrder: 'asc',
  },
};
