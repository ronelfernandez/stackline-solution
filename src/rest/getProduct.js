import data from '../data/data';
export default async id => {
  return data.find(p => p.id === id);
};
