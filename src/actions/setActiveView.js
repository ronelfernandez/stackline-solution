export const SET_ACTIVE_VIEW = 'SET_ACTIVE_VIEW';
export default activeView => {
  return {
    type: SET_ACTIVE_VIEW,
    activeView
  };
};
