import getProduct from '../rest/getProduct'
export const GET_PRODUCT = "GET_PRODUCT";
export default id => {
  return async dispatch => {
    try {
      const product = await getProduct(id);
      dispatch({
        type: GET_PRODUCT,
        payload: product,
      });
      return true;
    } catch (err) {
      return false;
    }
  };
};