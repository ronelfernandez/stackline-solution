export const SET_SORT_CONFIG = 'SET_SORT_CONFIG';
export const ASCENDING = 'asc';

export default sortColumn => {
  return {
    type: SET_SORT_CONFIG,
    sortColumn,
  };
};
